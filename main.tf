# Locals

locals {
  security_groups           = "${split(",", var.load_balancer_type == "application" ? join(",", var.security_groups) : "")}"
  idle_timeout              = "${var.load_balancer_type == "application" ? var.idle_timeout : "" }"
  enable_http2              = "${var.load_balancer_type == "application" ? var.enable_http2 : "" }"
  access_logs_enabled       = "${var.load_balancer_type == "application" && var.access_logs_enabled ? var.access_logs_enabled : "false" }"
  access_logs_bucket        = "${var.load_balancer_type == "application" && var.access_logs_enabled ? var.access_logs_bucket : "" }"
  custom_access_logs_prefix = "${format("%s-%s", var.name, var.resource_identities["lb"])}"
  access_logs_prefix        = "${var.access_logs_prefix != "" ? var.access_logs_prefix : local.custom_access_logs_prefix}"
  lb_id                     = "${element(concat(aws_lb.main_access_logs_present.*.id, aws_lb.main_access_logs_missing.*.id, aws_lb.main_network.*.id, list("")), 0)}"
  lb_arn                    = "${element(concat(aws_lb.main_access_logs_present.*.arn, aws_lb.main_access_logs_missing.*.arn, aws_lb.main_network.*.arn, list("")), 0)}"
}

## Resource 

# LB resource

resource "aws_lb" "main_access_logs_missing" {
  count = "${var.create_lb && !local.access_logs_enabled && var.load_balancer_type == "application" ? 1 : 0}"

  name                             = "${format("%s-%s", var.name, var.resource_identities["lb"],)}"
  internal                         = "${var.internal}"
  load_balancer_type               = "${var.load_balancer_type}"
  security_groups                  = ["${local.security_groups}"]
  subnets                          = ["${var.subnets}"]
  idle_timeout                     = "${local.idle_timeout}"
  enable_deletion_protection       = "${var.enable_deletion_protection}"
  enable_cross_zone_load_balancing = "${var.enable_cross_zone_load_balancing}"
  enable_http2                     = "${local.enable_http2}"
  ip_address_type                  = "${var.ip_address_type}"

  tags = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["lb"],)), var.global_tags, var.module_tags, var.lb_tags)}"
}

resource "aws_lb" "main_access_logs_present" {
  count = "${var.create_lb && local.access_logs_enabled && var.load_balancer_type == "application" ? 1 : 0}"

  name                             = "${format("%s-%s", var.name, var.resource_identities["lb"],)}"
  internal                         = "${var.internal}"
  load_balancer_type               = "${var.load_balancer_type}"
  security_groups                  = ["${local.security_groups}"]
  subnets                          = ["${var.subnets}"]
  idle_timeout                     = "${local.idle_timeout}"
  enable_deletion_protection       = "${var.enable_deletion_protection}"
  enable_cross_zone_load_balancing = "${var.enable_cross_zone_load_balancing}"
  enable_http2                     = "${local.enable_http2}"
  ip_address_type                  = "${var.ip_address_type}"

  access_logs {
    bucket  = "${local.access_logs_bucket}"
    enabled = "${local.access_logs_enabled}"
    prefix  = "${local.access_logs_prefix}"
  }

  tags = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["lb"],)), var.global_tags, var.module_tags, var.lb_tags)}"
}

resource "aws_lb" "main_network" {
  count = "${var.create_lb && !local.access_logs_enabled && var.load_balancer_type == "network" ? 1 : 0}"

  name                             = "${format("%s-%s", var.name, var.resource_identities["lb"],)}"
  internal                         = "${var.internal}"
  load_balancer_type               = "${var.load_balancer_type}"
  subnets                          = ["${var.subnets}"]
  enable_deletion_protection       = "${var.enable_deletion_protection}"
  enable_cross_zone_load_balancing = "${var.enable_cross_zone_load_balancing}"
  enable_http2                     = "${local.enable_http2}"
  ip_address_type                  = "${var.ip_address_type}"

  tags = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["lb"],)), var.global_tags, var.module_tags, var.lb_tags)}"
}

# Listeners

resource "aws_lb_listener" "forward_listener" {
  count = "${var.create_lb && length(var.forward_listener) > 0 ? length(var.forward_listener) : 0}"

  load_balancer_arn = "${local.lb_arn}"
  port              = "${lookup(var.forward_listener[count.index], "port", "80")}"
  protocol          = "${lookup(var.forward_listener[count.index], "protocol", "HTTP")}"
  ssl_policy        = "${lookup(var.forward_listener[count.index], "protocol", "HTTP") == "HTTPS" ? lookup(var.forward_listener[count.index], "ssl_policy", var.listener_ssl_policy_default) : ""}"
  certificate_arn   = "${lookup(var.forward_listener[count.index], "protocol", "HTTP") == "HTTPS" ? lookup(var.forward_listener[count.index], "certificate_arn", "") : ""}"

  default_action {
    type             = "${lookup(var.forward_listener[count.index], "type", "forward")}"
    target_group_arn = "${lookup(var.forward_listener[count.index], "target_group_arn", var.default_target_group_arn)}"
  }
}

resource "aws_lb_listener" "redirect_listener" {
  count = "${var.create_lb && length(var.redirect_listener) > 0 ? length(var.redirect_listener) : 0}"

  load_balancer_arn = "${local.lb_arn}"
  port              = "${lookup(var.redirect_listener[count.index], "port", "80")}"
  protocol          = "${lookup(var.redirect_listener[count.index], "protocol", "HTTP")}"
  ssl_policy        = "${lookup(var.redirect_listener[count.index], "protocol", "HTTP") == "HTTPS" ? lookup(var.redirect_listener[count.index], "ssl_policy", var.listener_ssl_policy_default) : ""}"
  certificate_arn   = "${lookup(var.redirect_listener[count.index], "protocol", "HTTP") == "HTTPS" ? lookup(var.redirect_listener[count.index], "certificate_arn", "") : ""}"

  default_action {
    type = "${lookup(var.redirect_listener[count.index], "type", "redirect")}"

    redirect {
      host        = "${lookup(var.redirect_listener[count.index], "host", "#{host}")}"
      path        = "${lookup(var.redirect_listener[count.index], "path", "/#{path}")}"
      query       = "${lookup(var.redirect_listener[count.index], "query", "")}"
      port        = "${lookup(var.redirect_listener[count.index], "to_port", "")}"
      protocol    = "${lookup(var.redirect_listener[count.index], "to_protocol", "")}"
      status_code = "${lookup(var.redirect_listener[count.index], "status_code", "HTTP_301")}"
    }
  }
}

resource "aws_lb_listener" "fixed_listener" {
  count = "${var.create_lb && length(var.fixed_listener) > 0 ? length(var.fixed_listener) : 0}"

  load_balancer_arn = "${local.lb_arn}"
  port              = "${lookup(var.fixed_listener[count.index], "port", "80")}"
  protocol          = "${lookup(var.fixed_listener[count.index], "protocol", "HTTP")}"
  ssl_policy        = "${lookup(var.fixed_listener[count.index], "protocol", "HTTP") == "HTTPS" ? lookup(var.fixed_listener[count.index], "ssl_policy", var.listener_ssl_policy_default) : ""}"
  certificate_arn   = "${lookup(var.fixed_listener[count.index], "protocol", "HTTP") == "HTTPS" ? lookup(var.fixed_listener[count.index], "certificate_arn", "") : ""}"

  default_action {
    type = "${lookup(var.fixed_listener[count.index], "type", "fixed-response")}"

    fixed_response {
      content_type = "${lookup(var.fixed_listener[count.index], "content_type", "text/plain")}"
      message_body = "${lookup(var.fixed_listener[count.index], "message_body", "Developed by Project Waffle")}"
      status_code  = "${lookup(var.fixed_listener[count.index], "status_code", "200")}"
    }
  }
}
