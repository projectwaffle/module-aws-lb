## Global variables

variable "resource_identities" {
  description = "Resource Identity according to PW Arhitecture Name Convention"
  type        = "map"
}

variable "global_tags" {
  description = "Global Tags"
  type        = "map"
}

## Module Specific variables

variable "module_tags" {
  description = "Module Tags"
  default     = {}
}

variable "name" {
  description = "Name for all resources to start with"
}

## Resource variables

# LB Resource

variable "create_lb" {
  description = "Set to false if you do not want to create LB for module"
  default     = "true"
}

variable "internal" {
  description = "If true, the LB will be internal"
  default     = "false"
}

variable "load_balancer_type" {
  description = " The type of load balancer to create. Possible values are application or network."
  default     = "application"
}

variable "security_groups" {
  description = "A list of security group IDs to assign to the LB. Only valid for Load Balancers of type application"
  default     = []
}

variable "subnets" {
  description = "A list of subnet IDs to attach to the LB. Subnets cannot be updated for Load Balancers of type network. Changing this value for load balancers of type network will force a recreation of the resource."
  default     = []
}

variable "idle_timeout" {
  description = "The time in seconds that the connection is allowed to be idle. Only valid for Load Balancers of type application. Default: 60"
  default     = "60"
}

variable "enable_deletion_protection" {
  description = "If true, deletion of the load balancer will be disabled via the AWS API. This will prevent Terraform from deleting the load balancer."
  default     = "false"
}

variable "enable_cross_zone_load_balancing" {
  description = "If true, cross-zone load balancing of the load balancer will be enabled. This is a network load balancer feature."
  default     = "true"
}

variable "enable_http2" {
  description = "Indicates whether HTTP/2 is enabled in application load balancers"
  default     = "true"
}

variable "ip_address_type" {
  description = "The type of IP addresses used by the subnets for your load balancer. The possible values are ipv4 and dualstack"
  default     = "ipv4"
}

variable "access_logs_enabled" {
  description = "Boolean to enable / disable access_logs. Defaults to false, even when bucket is specified"
  default     = "false"
}

variable "access_logs_bucket" {
  description = "The S3 bucket name to store the logs in"
  default     = ""
}

variable "access_logs_prefix" {
  description = "The S3 bucket prefix. Logs are stored in the root if not configured"
  default     = ""
}

variable "lb_tags" {
  description = "Custome KMS Tags"
  default     = {}
}

# LB Listener

variable "default_target_group_arn" {
  description = "Default Target Group ARN for input"
  default     = ""
}

variable "listener_ssl_policy_default" {
  description = "Default SSL Policy to use"
  default     = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
}

variable "forward_listener" {
  description = "Forward Listener Details. Avaiable settings `port`, `protocol`, `ssl_policy`, `certificate_arn` or `target_group_arn` other than `var.default_target_group_arn`"
  default     = []
}

variable "redirect_listener" {
  description = "Redirect Listener. Avaiable settings `host`, `path`, `query`, `port` - port to redirect to - ex 443, `protocol` - protocol to redirect to - ex HTTPS, `status_code` - HTTP codes to return - permanent (HTTP_301) or temporary (HTTP_302)"
  default     = []
}

variable "fixed_listener" {
  description = "Fixed-response Listener. Avaiable settings `content_type` - The content type. Valid values are text/plain, text/css, text/html, application/javascript and application/json, `message_body`, `status_code` - The HTTP response code. Valid values are 2XX, 4XX, or 5XX."
  default     = []
}
