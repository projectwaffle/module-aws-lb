# AWS Load balancer Terraform module

[![CircleCI](https://circleci.com/bb/projectwaffle/module-aws-lb.svg?style=svg)](https://circleci.com/bb/projectwaffle/module-aws-lb/)

Terraform module which creates LB resources on AWS. Might be used for different modules

These types of resources are supported:

* [LB](https://www.terraform.io/docs/providers/aws/r/lb.html)
* [LB listener](https://www.terraform.io/docs/providers/aws/r/lb_listener.html)

Sponsored by [Draw.io - the best way to draw AWS diagrams](https://www.draw.io/)

## Usage

```hcl
module "app_lb" {
  source = "bitbucket.org/projectwaffle/module-aws-lb.git?ref=tags/v0.0.1"

  name = "training-prod"

  resource_identities = {
    lb = "app-lb"
  }

  global_tags = "${var.global_tags}"

  create_lb       = "true"
  security_groups = ["${module.sg_lb.security_group_id}"]
  subnets         = "${module.vpc.public_subnets}"

  access_logs_enabled = "true"
  access_logs_bucket  = "${module.s3_lb.s3_bucket_name}"

  default_target_group_arn = "${module.tg_lb.default_target_group_arn}"


  forward_listener = [
    {
         port = "8080"
    },
    {
      port            = "443"
      protocol        = "HTTPS"
      certificate_arn = "arn:aws:acm:eu-west-1:795189792122:certificate/b4aea056-45aa-4561-a9a1-79c4f46464e1"
    },
  ]


  redirect_listener = [
    {
      port        = "80"
      to_port     = "443"
      protocol    = "HTTP"
      to_protocol = "HTTPS"
      status_code = "HTTP_301"
    },
  ]

  fixed_listener = [
    {
      port            = "8443"
      protocol        = "HTTPS"
      certificate_arn = "arn:aws:acm:eu-west-1:795189792122:certificate/b4aea056-45aa-4561-a9a1-79c4f46464e1"
      content_type = "text/html"
      message_body = "${file("./index.html")}"
      status_code = "200"
    },
  ]
}

```

## Resource Identity

In order to add custom names to resources, each module has two variables to be used for this.
* Variable called `name` - this will be used as the base name for each resource in the module
    * `name = "training-prod"` 
* MAP variable called `resource_identities` - used to append specific name to each resoruce
```hcl
  resource_identities = {
    lb = "app-lb"
  }
```

Both variables should be declared in module section: 
```hcl
module "config" {
  ......
  name = "training-prod"
  resource_identities = {
    lb = "app-lb"
  }
  ......
}  
```

Next is an example of variable used in the Module resoruce names or tgas 'Name':
```hcl
  name  = "${format("%s-%s", var.name, var.resource_identities["lb"])}"
  tags  = "${merge(map("Name", format("%s-%s", var.name, var.resource_identities["lb"],)), var.global_tags, var.module_tags, var.lb_tags)}"
```


## Resource Tagging

There are three level of resource tagging declaration
* Global level - declare tagging strategy that will be applied on all the AWS resources

```hcl
variable "global_tags" {
  type = "map"

  default = {
    environment     = "prod"
    role            = "infrastructure"
    version         = "0.1"
    owner           = "Sergiu Plotnicu"
    bu              = "IT"
    customer        = "project waffle"
    project         = "training"
    confidentiality = "open"
    compliance      = "N/A"
    encryption      = "disabled"
  }
}
```

* Module level - taggig that will affect only resources created by the module itself
```hcl
module "kms" {

  module_tags = {
    bu = "security"
  }
  global_tags         = "${var.global_tags}"
}
```

* Resource level - each resource can be tagged with custome tag or tage that will rewrite all the others one

```hcl
variable "kms_tags" {
  description = "Custome KMS Tags"
  default     = {
    role = "encryption"
  }
}
```

Combination of all this variables should cover all the tagging requirements. 
Tags are beeing merged so, resource level ones, will rewrite modules one, that will rewrite global ones.

## Terraform version

Terraform version 0.11.10 or newer is required for this module to work.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| access\_logs\_bucket | The S3 bucket name to store the logs in | string | `` | no |
| access\_logs\_enabled | Boolean to enable / disable access_logs. Defaults to false, even when bucket is specified | string | `false` | no |
| access\_logs\_prefix | The S3 bucket prefix. Logs are stored in the root if not configured | string | `` | no |
| create\_lb | Set to false if you do not want to create LB for module | string | `true` | no |
| default\_target\_group\_arn | Default Target Group ARN for input | string | `` | no |
| enable\_cross\_zone\_load\_balancing | If true, cross-zone load balancing of the load balancer will be enabled. This is a network load balancer feature. | string | `true` | no |
| enable\_deletion\_protection | If true, deletion of the load balancer will be disabled via the AWS API. This will prevent Terraform from deleting the load balancer. | string | `false` | no |
| enable\_http2 | Indicates whether HTTP/2 is enabled in application load balancers | string | `true` | no |
| fixed\_listener | Fixed-response Listener. Avaiable settings `content_type` - The content type. Valid values are text/plain, text/css, text/html, application/javascript and application/json, `message_body`, `status_code` - The HTTP response code. Valid values are 2XX, 4XX, or 5XX. | list | `<list>` | no |
| forward\_listener | Forward Listener Details. Avaiable settings `port`, `protocol`, `ssl_policy`, `certificate_arn` or `target_group_arn` other than `var.default_target_group_arn` | list | `<list>` | no |
| global\_tags | Global Tags | map | - | yes |
| idle\_timeout | The time in seconds that the connection is allowed to be idle. Only valid for Load Balancers of type application. Default: 60 | string | `60` | no |
| internal | If true, the LB will be internal | string | `false` | no |
| ip\_address\_type | The type of IP addresses used by the subnets for your load balancer. The possible values are ipv4 and dualstack | string | `ipv4` | no |
| lb\_tags | Custome KMS Tags | map | `<map>` | no |
| listener\_ssl\_policy\_default | Default SSL Policy to use | string | `ELBSecurityPolicy-TLS-1-2-Ext-2018-06` | no |
| load\_balancer\_type | The type of load balancer to create. Possible values are application or network. | string | `application` | no |
| module\_tags | Module Tags | map | `<map>` | no |
| name | Name for all resources to start with | string | - | yes |
| redirect\_listener | Redirect Listener. Avaiable settings `host`, `path`, `query`, `port` - port to redirect to - ex 443, `protocol` - protocol to redirect to - ex HTTPS, `status_code` - HTTP codes to return - permanent (HTTP_301) or temporary (HTTP_302) | list | `<list>` | no |
| resource\_identities | Resource Identity according to PW Arhitecture Name Convention | map | - | yes |
| security\_groups | A list of security group IDs to assign to the LB. Only valid for Load Balancers of type application | list | `<list>` | no |
| subnets | A list of subnet IDs to attach to the LB. Subnets cannot be updated for Load Balancers of type network. Changing this value for load balancers of type network will force a recreation of the resource. | list | `<list>` | no |

## Outputs

| Name | Description |
|------|-------------|
| lb\_arn | Created LB ARN |
| lb\_arn\_suffix | Created LB ARN Suffix |
| lb\_dns\_name | Created LB DNS Name |
| lb\_id | Created LB ID |
| lb\_name | Created LB Name |
| lb\_zone\_id | Created LB Zone ID |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->


## Authors

Module is maintained by [Sergiu Plotnicu](https://bitbucket.org/projectwaffle/)

## License

Licensed under Sergiu Plotnicu, please contact him at - sergiu.plotnicu@gmail.com


