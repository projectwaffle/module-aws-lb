output "lb_id" {
  description = "Created LB ID"
  value       = "${element(concat(aws_lb.main_access_logs_missing.*.id, aws_lb.main_access_logs_present.*.id, aws_lb.main_network.*.id, list("")), 0)}"
}

output "lb_arn" {
  description = "Created LB ARN"
  value       = "${element(concat(aws_lb.main_access_logs_missing.*.arn, aws_lb.main_access_logs_present.*.arn, aws_lb.main_network.*.arn, list("")), 0)}"
}

output "lb_arn_suffix" {
  description = "Created LB ARN Suffix"
  value       = "${element(concat(aws_lb.main_access_logs_missing.*.arn_suffix, aws_lb.main_access_logs_present.*.arn_suffix, aws_lb.main_network.*.arn_suffix, list("")), 0)}"
}

output "lb_name" {
  description = "Created LB Name"
  value       = "${element(concat(aws_lb.main_access_logs_missing.*.name, aws_lb.main_access_logs_present.*.name, aws_lb.main_network.*.name, list("")), 0)}"
}

output "lb_dns_name" {
  description = "Created LB DNS Name"
  value       = "${element(concat(aws_lb.main_access_logs_missing.*.dns_name, aws_lb.main_access_logs_present.*.dns_name, aws_lb.main_network.*.dns_name, list("")), 0)}"
}

output "lb_zone_id" {
  description = "Created LB Zone ID"
  value       = "${element(concat(aws_lb.main_access_logs_missing.*.zone_id, aws_lb.main_access_logs_present.*.zone_id, aws_lb.main_network.*.zone_id, list("")), 0)}"
}
